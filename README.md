# Block Lines

*It's a web service to allow users to, when invoking an API method (POST), to write on a shared log file such that each entry (line) is linked to the previous one using it's hash and a proof of work.*

### Table of Contents

- [Usage](#usage)
- [Terms & Concepts](#terms-and-concepts)

## Usage

To use this project, run the following commands in your terminal:

```bash
git clone git@gitlab.com:publicrepos1/block-lines.git && cd block-lines
yarn install
yarn start
```

You should see output indicating that the genesis block was created and that the web server is listening for HTTP requests:

```bash
2022-06-30T01:46:39.768Z: 0000000000000000000000000000000000000000000000000000000000000000,Hola Mundo,5
Webserver listening on: http://0.0.0.0:4041/blocks
```

In another terminal, run the following command to execute the unit tests. You should see something like this:

```bash
yarn run test

  # POST /blocks
    ✓ should return BAD_REQUEST status for missing required field in body
    ✓ should create a Block with message: Chau Mundo
    ✓ should create a Block with message: Adeu Mundo
    ✓ should create a Block with message: Bye Mundo
    ✓ should create a Block with message: Sayonara Mundo

  # GET /blocks
    ✓ should be a cryptographic link between all blocks
    ✓ should be a common prefix '00' on all block hashes

  # CSV log file
    ✓ should take a few seconds for all the requests to be completed (2003ms)
    ✓ should exist a shared log file at: '/Users/lgomez/gits/block-lines/src/log/blocks.csv'
line: 0000000000000000000000000000000000000000000000000000000000000000,Hola Mundo,5
line: 0038711c83bd05b1e369e27246df4ba815a6dda04116b1b2f9a8c21ba4e1de38,Chau Mundo,71
line: 00232c7d3c2283695a4029eddc1b9e8c83914515832a04f57b402fc444aa11b5,Adeu Mundo,270
line: 006e73c3aac85e140aed835b94d5325ebf5cda0cc142937de8bf46265b92197c,Bye Mundo,564
line: 002faf96ffc316d6c4b90c554faa1df16402826bc343cb03f0285160eecc3431,Sayonara Mundo,351
    ✓ should be that each line has the format: <prev_hash>,<message>,<nonce>


  10 passing (2s)
```


## Terms and Concepts

We are building a simple but very illustrative example on how to build a blockchain with JavaScript.

Further reading on the following topics is recommended.

- [**Proof of Work:**]() it’s a cryptographic puzzle that, based on the dispersion (randomness) of hash functions, can be used to easily check a certain amount of work has been made as there is no other way to solve it besides doing bruteforce look up. It initially was created as a spam filter mechanism and nowadays is heavily used in blockchain applications.

- A consecuence of [**hash-linking entries**]() is that if anyone wants to edit one, all the hashes after it might be recalculated, i.e, all the work already made needs to be done again therefore it’s harder (in terms of computing power) to change old entries. This is a way to encourage immutability.


## MIT License
This project is open source for anyone who wants to learn about block chain.

## Developer
Luis Alonso Gómez – luisalonsogg@gmail.com
