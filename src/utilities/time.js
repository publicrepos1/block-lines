import moment from "moment"
import timezone from "moment-timezone"

export const timestamp = (time) => moment(time).valueOf()
export const dateTime = (time) => timezone(time).tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss")
export const dateTimestamp = (time) => timezone(time).tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss.SSS")
export const uniqueTimeId = (time) => timezone(time).tz("America/Los_Angeles").format("YYYY_MM_DD_HH_mm_ss_SSS")
export const durationSince = (since) => moment.duration(moment().valueOf() - since)
export const durationSinceTime = (time, since) => moment.duration(time - since)

export const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms))
