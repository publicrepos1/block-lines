import { createLogger, format, transports } from "winston"

export default class Logger {
    constructor(config) {
        this.config = config
        this.logger = createLogger({
            transports: [
                new transports.Console({
                    json: false,
                    format: format.combine(
                        format.colorize(),
                        format.timestamp(),
                        format.prettyPrint(),
                        format.printf(({ csvLine, timestamp }) => `${timestamp}: ${csvLine}`)
                    )
                }),
                new transports.File({
                    json: false,
                    filename: this.config.fileName,
                    format: format.combine(
                        format.simple(),
                        format.timestamp(),
                        format.prettyPrint(),
                        format.printf(({ csvLine }) => `${csvLine}`)
                    )
                })
            ]
        })
    }

    append(csvLine) {
        this.logger.log({ level: "info", csvLine })
    }
}
