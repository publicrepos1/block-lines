import crypto from "crypto"
import fs from "fs"
import Block from "./Block"

export default class BlockMiner {
    constructor(config, logger) {
        this.config = config
        this.logger = logger
        this.chain = []
        this.initializeChain()
        this.createGenesisBlock()
    }

    createGenesisBlock(message = "Hola Mundo") {
        const prev_hash = "0000000000000000000000000000000000000000000000000000000000000000"
        const { hash, nonce } = this.mineBlock(prev_hash, message)
        const genesisBlock = new Block(message, hash, nonce, prev_hash)
        this.chain.push(genesisBlock)
        this.appendBlockToFile(genesisBlock)
        return genesisBlock
    }

    appendBlock(message) {
        const latestBlock = this.chain[this.chain.length - 1]
        const { hash, nonce } = this.mineBlock(latestBlock.hash, message)
        const newBlock = new Block(message, hash, nonce, latestBlock.hash)
        this.chain.push(newBlock)
        this.appendBlockToFile(newBlock)
        return newBlock
    }

    appendBlockToFile(block) {
        this.logger.append(block.toCSV())
    }

    getHash(prev_hash, message, nonce) {
        return crypto.createHash("sha256").update(`${prev_hash},${message},${nonce}`, "utf8").digest("hex").toString()
    }

    mineBlock(prev_hash, message) {
        let nonce = 0
        while (true) {
            const hash = this.getHash(prev_hash, message, nonce)
            if (hash.startsWith("00")) return { hash, nonce }
            nonce++
        }
    }

    initializeChain() {
        if (fs.existsSync(this.config.fileName)) fs.unlinkSync(this.config.fileName)
    }

    getBlocks() {
        return this.chain
    }
}
