export default class Block {
    constructor(message, hash, nonce, prev_hash) {
        this.message = message
        this.hash = hash
        this.nonce = nonce
        this.prev_hash = prev_hash
    }

    toCSV() {
        return `${this.prev_hash},${this.message},${this.nonce}`
    }
}
