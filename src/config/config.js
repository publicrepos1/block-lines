import Joi from "joi"
import path from "path"

// define validation for all the env vars
const envVarsSchema = Joi.object({
    APP_VERSION: Joi.string().default("0.0.1"),
    DEBUG: Joi.boolean().default(false),
    NODE_ENV: Joi.string().allow(["development", "production", "test", "provision"]).default("production"),
    PORT: Joi.number().default(4041),
    HOST: Joi.string().default("0.0.0.0")
})
    .unknown()
    .required()

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}
const fileName = path.resolve(__dirname, "../log/blocks.csv")

const config = {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
    host: envVars.HOST,
    connectionString: `http://${envVars.HOST}:${envVars.PORT}`,
    fileName
}

export default config
