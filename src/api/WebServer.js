import BlockHttp from "./post/BlockHttp"
import config from "../config/config"
import Http from "./Http"
import Logger from "../utilities/logger"
import BlockMiner from "../miner/BlockMiner"

export default class WebServer {
    constructor() {
        this.config = config
        this.logger = undefined
        this.blockHttp = undefined
        this.blockMiner = undefined
    }

    boot() {
        this.createWebserverInstance().start()
    }

    createWebserverInstance() {
        return new Http(this.getConfig(), this.getBlockHttp())
    }

    getConfig() {
        return this.config
    }

    getLogger() {
        if (this.logger) return this.logger
        this.logger = new Logger(this.getConfig())
        return this.logger
    }

    getBlockMiner() {
        if (this.blockMiner) return this.blockMiner
        this.blockMiner = new BlockMiner(this.getConfig(), this.getLogger())
        return this.blockMiner
    }

    getBlockHttp() {
        if (this.blockHttp) return this.blockHttp
        this.blockHttp = new BlockHttp(this.getBlockMiner())
        return this.blockHttp
    }
}
