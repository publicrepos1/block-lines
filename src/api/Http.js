import bodyParser from "body-parser"
import createError from "http-errors"
import express from "express"
import expressWinston from "express-winston"
import helmet from "helmet"
import http from "http"
import httpStatus from "http-status"
import logger from "morgan"
import timeout from "connect-timeout"
import path from "path"
import winston from "winston"
import { dateTime } from "../utilities/time"

export default class Http {
    constructor(config, blockHttp) {
        this.config = config
        this.blockHttp = blockHttp
    }

    start() {
        const app = express()
        if (this.config.env === "development") {
            app.use(logger("dev"))

            // this will let us get the data from a POST or attach data to req.body
            app.use(bodyParser.json())
            app.use(bodyParser.urlencoded({ extended: true }))

            // enable detailed API logging in dev env
            expressWinston.requestWhitelist.push("body")
            expressWinston.responseWhitelist.push("body")
            app.use(
                expressWinston.logger({
                    transports: [
                        new winston.transports.File({
                            filename: path.resolve(__dirname, "../log/api.log"),
                            format: winston.format.combine(
                                winston.format.timestamp(),
                                winston.format.prettyPrint(),
                                winston.format.printf(
                                    ({ level, message, label, timestamp }) => `${timestamp} ${level}: ${message}`
                                )
                            )
                        }),
                        new winston.transports.Console({
                            format: winston.format.combine(
                                winston.format.colorize(),
                                winston.format.timestamp(),
                                winston.format.prettyPrint(),
                                winston.format.printf(
                                    ({ level, message, label, timestamp }) => `${timestamp} ${level}: ${message}`
                                )
                            )
                        })
                    ],
                    meta: false, // optional: log meta data about request (defaults to true)
                    msg: "HTTP | Method: {{req.method}} | URL: {{req.url}} | Status: {{res.statusCode}} | Response Time: {{res.responseTime}}ms",
                    colorStatus: true // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
                })
            )
        } else if (this.config.env === "production") {
            app.use(
                logger((tokens, req, res) =>
                    [
                        dateTime(tokens.date(req, res)),
                        "| Method:",
                        tokens.method(req, res),
                        "| URL:",
                        tokens.url(req, res).split("?")[0],
                        "| Status:",
                        tokens.status(req, res),
                        "| Response Length:",
                        tokens.res(req, res, "content-length"),
                        "| Response Time:",
                        tokens["response-time"](req, res),
                        "ms",
                        "| Message:",
                        tokens.req(req, res, "message")
                        // '| IP address:',
                        // tokens['remote-addr'](req, res)
                    ].join(" ")
                )
            )
        }
        app.use(timeout("120s"))
        app.use(helmet())
        app.use(express.urlencoded({ extended: true }))

        app.get("/", (request, response, next) => {
            response.sendStatus(httpStatus.NO_CONTENT)
        })

        app.post("/blocks", async (request, response, next) => {
            if (request.timedout) return next(createError(httpStatus.REQUEST_TIMEOUT, "Service Unavailable"))
            const res = await this.blockHttp.postBlockLine(request)
            if (createError.isHttpError(res)) return next(res)
            response.send(res)
        })

        app.get("/blocks", async (request, response, next) => {
            if (request.timedout) return next(createError(httpStatus.REQUEST_TIMEOUT, "Service Unavailable"))
            const res = await this.blockHttp.getBlocks()
            if (createError.isHttpError(res)) return next(res)
            response.send(res)
        })

        const errorFilter = (err, req, res, next) => {
            if (!res.headersSent) {
                const errcode = err.status || 500
                const msg = err.message || "Internal Server error."
                res.status(errcode).send({ notification: msg })
            }
        }

        app.use(errorFilter)
        http.createServer({}, app).listen(this.config.port, this.config.host)

        console.log(`Webserver listening on: http://${this.config.host}:${this.config.port}/blocks`)
    }
}
