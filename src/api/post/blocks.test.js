import request from "supertest"
import httpStatus from "http-status"
import chai, { expect } from "chai"
import fs from "fs"
import path from "path"
import config from "../../config/config"
import { sleep } from "../../utilities/time"

chai.config.includeStack = true

describe("# POST /blocks", () => {
    it("should return BAD_REQUEST status for missing required field in body", (done) => {
        request(config.connectionString)
            .post("/blocks")
            .type("form")
            .set("Accept", "application/json")
            .send({ missing_message: "" })
            .expect("Content-Type", "application/json; charset=utf-8")
            .expect(httpStatus.BAD_REQUEST, { notification: "message is required" }, done)
    })

    it("should create a Block with message: Chau Mundo", (done) => {
        request(config.connectionString)
            .post("/blocks")
            .type("form")
            .set("Accept", "application/json")
            .send({ message: "Chau Mundo" })
            .expect(httpStatus.OK)
            .end((err, res) => {
                done()
            })
    })

    it("should create a Block with message: Adeu Mundo", (done) => {
        request(config.connectionString)
            .post("/blocks")
            .type("form")
            .set("Accept", "application/json")
            .send({ message: "Adeu Mundo" })
            .expect(httpStatus.OK)
            .end((err, res) => {
                done()
            })
    })

    it("should create a Block with message: Bye Mundo", (done) => {
        request(config.connectionString)
            .post("/blocks")
            .type("form")
            .set("Accept", "application/json")
            .send({ message: "Bye Mundo" })
            .expect(httpStatus.OK)
            .end((err, res) => {
                done()
            })
    })

    it("should create a Block with message: Sayonara Mundo", (done) => {
        request(config.connectionString)
            .post("/blocks")
            .type("form")
            .set("Accept", "application/json")
            .send({ message: "Sayonara Mundo" })
            .expect(httpStatus.OK)
            .end((err, res) => {
                done()
            })
    })
})

describe("# GET /blocks", () => {
    it("should be a cryptographic link between all blocks", async () => {
        const response = await request(config.connectionString).get("/blocks").expect(httpStatus.OK)
        const { body } = response
        const blocks = body
        for (let i = 0; i < blocks.length; i++) {
            if (i === 0)
                expect(blocks[i].prev_hash).to.equal("0000000000000000000000000000000000000000000000000000000000000000")
            else expect(blocks[i - 1].hash).to.equal(blocks[i].prev_hash)
        }
    })

    it("should be a common prefix '00' on all block hashes", async () => {
        const response = await request(config.connectionString).get("/blocks").expect(httpStatus.OK)
        const { body } = response
        const blocks = body
        for (let i = 0; i < blocks.length; i++) expect(blocks[i].hash.startsWith("00")).to.be.true
    })
})

describe("# CSV log file", () => {
    it("should take a few seconds for all the requests to be completed", async () => {
        await sleep(2000)
    })

    it(`should exist a shared log file at: '${config.fileName}'`, () => {
        expect(fs.existsSync(config.fileName)).to.be.true
    })

    it("should be that each line has the format: <prev_hash>,<message>,<nonce>", () => {
        const prev_blocks = []
        const data = fs.readFileSync(config.fileName, { encoding: "utf8" })
        expect(data.trim().length === 0).to.be.false
        const lines = data.split("\n")
        for (const line of lines) {
            if (line.trim().length === 0) continue
            console.log(`line: ${line}`)
            const [prev_hash, message, nonce] = line.split(",")
            expect(prev_hash.length === 64).to.be.true
            expect(message.length > 0).to.be.true
            expect(parseInt(nonce)).to.be.a("number")
        }
        return prev_blocks
    })
})
