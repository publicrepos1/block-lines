import createError from "http-errors"
import httpStatus from "http-status"

export default class BlockHttp {
    constructor(miner) {
        this.miner = miner
    }

    async postBlockLine(request) {
        const { message } = request.body
        if (!message) return createError(httpStatus.BAD_REQUEST, "message is required")
        try {
            this.miner.appendBlock(message)
        } catch (e) {
            return createError(httpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }

    async getBlocks() {
        try {
            return this.miner.getBlocks()
        } catch (e) {
            return createError(httpStatus.INTERNAL_SERVER_ERROR, e.message)
        }
    }
}
